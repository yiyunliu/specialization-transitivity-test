module Lib
  ( isEven )
where

{-# SPECIALIZE isEven :: Int -> Bool #-}
isEven :: Integral a => a -> Bool
isEven n
  | n < 0 = isEven (- n)
  | n > 0 = isOdd (n - 1)
  | otherwise = True

isOdd :: Integral a => a -> Bool
isOdd n
  | n < 0 = isEven n
  | otherwise = isOdd n
