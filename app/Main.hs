module Main where
import Lib (isEven)

main :: IO ()
main = do
         n <- readLn :: IO Int
         putStrLn $
           "Hello, Haskell!" ++
           "n is" ++
           if isEven n
             then "Even"
             else "Odd"
